# CV Plateform Application
-------------

This application uses NodeJs, MySql, and Pug.
NodeJs v11.15.0


## Installation :

Change your nodeJs version to v11.15.0

Install the dependencies : 'npm install'

--

In 'server.js ', verify port of server nodeJs (currently 8888)

Import 'cv_plateform.sql'

In 'config > database.js ', verify port of database Mysql (currently 3306)

Enter the user/password of database.

--

Run the server : 'node server.js'
