var express = require('express');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
var pug = require('pug');
var app = express();
var port = process.env.PORT || 8888;

var passport = require('passport');
var flash = require('connect-flash');

require('./config/passport')(passport);

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended : true}));

app.set('view engine', pug);

app.use(session({
  secret:'Cat',
  resave: true,
  saveUninitialized : true
}));

app.use(express.static(path.join(__dirname + '/public'))); //css
app.use(express.static(path.join(__dirname + '/views'))); //js client

app.use(function(req, res, next){
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});


app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./app/routes/routes.js')(app, passport);

app.listen(port);
console.log('Port: ' + port);
