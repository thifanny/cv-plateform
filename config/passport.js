var LocalStrategy = require('passport-local').Strategy;

var mysql = require('mysql');
var bcrypt = require('bcrypt');
var dbconfig = require('./database');

//Create connection to database
var connection = mysql.createConnection(dbconfig.connection);
//Connect to database
connection.connect(function(err){
  if(err){
    throw err
  }
  console.log('Connect to database')
});
global.connection = connection;


module.exports = function(passport){
  passport.serializeUser(function(user, done){
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done){
    connection.query('SELECT * FROM users WHERE id = ?', [id],
      function(err, rows){
        done(err, rows[0]);
      });
  });

  passport.use(
    'local-signup',
    new LocalStrategy({
      usernameField : 'user_email',
      passwordField : 'user_password',
      passReqToCallback : true
    },
    function(req, user_email, user_password, done){
      var regexPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
      var regexEmail = /^.+@[^\.].*\.[a-z]{2,}$/;

      if(!regexEmail.test(user_email) || !regexPassword.test(user_password)){
        return done(null, false, req.flash("signupMessage", "Le mot de passe et/ou l'identifiant n'est pas conforme"));
      }

      connection.query("SELECT * FROM users WHERE user_email = ? ",
      [user_email], function(err, rows){
        if(err){
          return done(err);
        }
        if(rows.length){
          return done(null, false, req.flash("signupMessage", "Identifiant déjà existant"));
        } else {

          var hash = bcrypt.hashSync(user_password, 5);

          var newUserMysql = {
            user_email : user_email,
            user_password : hash
          };

          var insertQuery = "INSERT INTO users (user_email, user_password) VALUES (?, ?)";

          connection.query(insertQuery, [newUserMysql.user_email, newUserMysql.user_password],
            function(err, result){
              if(err){
                return err
              } else {
                newUserMysql.id = result.insertId;
                return done(null, newUserMysql)
              }
            });
          }
      })
    })
  );

  passport.use(
    'local-login',
    new LocalStrategy({
      usernameField : 'user_email',
      passwordField : 'user_password',
      passReqToCallback : true
    },
    function(req, user_email, user_password, done){
      connection.query('SELECT * FROM users WHERE user_email = ?', [user_email],
      function(err, rows){
        if(err){
          return done(err)
        }
        if(!rows.length){
          return done(null, false, req.flash('loginMessage', 'Mot de passe et/ou identifiant erroné(s)'));
        }
        if(!bcrypt.compareSync(user_password, rows[0].user_password)){
          return done(null, false, req.flash('loginMessage', 'Mot de passe et/ou identifiant erroné(s)'));
        }

        return done(null, rows[0]);
      });
    })
  );

};
