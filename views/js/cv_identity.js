var cv_identity = (function(){

  var updateProfilePicture = function(){

    var btnValid = $('.btnValidUpdateProfilePicture');

    $("#cvI_profilPicture").change(function(){
      var logFile = $('#cvI_profilPicture').get(0).files[0];
      var size = logFile.size;
      if(size > 50000){
        $('#info').html('Le fichier ne doit pas dépasser les 50ko')
      } else{
        $('#info').html(' ');
        var reader = new FileReader;
        reader.readAsDataURL(logFile);

        reader.onload = function(e){
          var infos = {cvI_profilPicture : reader.result };
          //var image = new Image();
          //image.src = rawLog
          //$("#profilPicture").append(image);
          //console.log(rawLog);
          btnValid.on('click', function(e){
            var id_cv = e.target.id;
            var urlUpdate = '/profile/cv/photo/:' + id_cv;
            var url = '/profile/cv/:' + id_cv;

            $.ajax({
              url: urlUpdate,
              type: 'PUT',
              data: infos,
              success: function(req, res, data) {
                console.log(res);
                if(res == 'success'){
                  window.location.href = url;
                }
              }
            });
          })
        }
      };
    });
  }

  var updateTitle = function(){
    var btnUpdateTitle = $('.btnUpdateTitle');
    var btnValidUpdateTitle= $('.btnValidUpdateTitle');

    btnValidUpdateTitle.on('click', function(e){
      var cv_title = $('#cv_title').val();
      var info = {cv_title : cv_title};

      var id_cv = e.target.id;
      var urlUpdateTitle = '/profile/cv/title/:' + id_cv;
      var returnUpdate = '/profile/cv/:' + id_cv;
      $.ajax({
        url: urlUpdateTitle,
        type: 'PUT',
        data: info,
        success: function(req, res, data) {
          console.log(res);
          if(res == 'success'){
            window.location.href = returnUpdate;
          }
        }
      });

    })
  }

  var updateCv = function(){
    var btnUpdateCv = $('.btnUpdateCv');
    var btnValidUpdate = $('.btnValidUpdate')

    btnValidUpdate.on('click', function(e){
      var cvI_name = $('#cvI_name').val();
      var cvI_email = $('#cvI_email').val();
      var cvI_website = $('#cvI_website').val();
      var cvI_phoneNumber = $('#cvI_phoneNumber').val();

      var infos = {
        cvI_name : cvI_name,
        cvI_email : cvI_email,
        cvI_website : cvI_website,
        cvI_phoneNumber : cvI_phoneNumber,
      };

      var id_cv = e.target.id;
      var urlUpdate = '/profile/cv/:' + id_cv;
      $.ajax({
        url: urlUpdate,
        type: 'PUT',
        data: infos,
        success: function(req, res, data) {
          console.log(res);
          if(res == 'success'){
            window.location.href = urlUpdate;
          }
        }
      });
    });
  };

  var deleteCv = function(){
    var btnsDeleteCv = $('.btnsDeleteCv');

    btnsDeleteCv.on('click', function(e){
      if(confirm('Etes-vous sûr de vouloir supprimer ce CV ?')){
        var id_cv = e.target.id;
        var urlDelete = '/profile/cv/:' + id_cv;
        $.ajax({
          url: urlDelete,
          type: 'DELETE',
          success: function(req, res, data) {
            console.log(res);
            if(res == 'success'){
              window.location.href = '/profile';
            }
          }
        });
      }
    })
  }

  var backToProfile = function(){
    var btnBacktoProfile = $('#btnBacktoProfile');
    btnBacktoProfile.on('click', function(){
        window.location.href='/profile';
    })
  };

  var dialog_box = function(){
    /* TO DO :
    $('#identityModal').on('shown.bs.modal', function () {
      $('#cvI_name').trigger('focus')
    })
    */
  };

  return {
    updateProfilePicture : updateProfilePicture,
    deleteCv : deleteCv,
    updateTitle : updateTitle,
    updateCv : updateCv,
    backToProfile : backToProfile,
    dialog_box : dialog_box
  };

})();

cv_identity.updateProfilePicture();
cv_identity.deleteCv();
cv_identity.updateTitle();
cv_identity.updateCv();
cv_identity.backToProfile();
cv_identity.dialog_box();
