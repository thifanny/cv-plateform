var cv_skills = (function(){

  var moreDetails = function(){

    $("#btnMoreDetails").on('click', function(e){
      $('<input type="text" class="form-control cvS_d">').insertBefore('#details button');
      $('#details input').each(function(i){
        var cvS_d = $(this).attr('id', 'cvS_details' + i);
        var cvS_detail = $(this).val();
        i++;
      })
    })
  }

  // Post new cv_work
  var postCvSkills = function(){
    var btnAddCSkills = $('.btnAddCSkills');

    btnAddCSkills.on('click', function(e){
      var id_cv = e.target.id;
      var btnValidSkills = $('.btnValidSkills');

      btnValidSkills.on('click', function(e){
        var cvS_name = $('#cvS_name').val();
        var cvS_details = [];

        $( "#details input" ).each(function() {
          cvS_details.push($(this).val());
        });

        var infos = {
          cvS_name : cvS_name,
          cvS_detail : cvS_details
        };

        var urlPost = '/profile/cv/skills/:' + id_cv;
        var url = '/profile/cv/:' + id_cv
        $.post(urlPost, infos, function(req, res, data){
          if(res == 'success'){
            window.location.href = url;
          }
        });
      });
    });
  };

  var updateCvSkills = function(){

    var btnUpdateCvSkills = $('.btnUpdateCvSkills');
    var cvS_details = [];

    btnUpdateCvSkills.on('click', function(e){
      var id_cvS = e.target.id;
      var btnValidSkills = $('.btnValidSkills');

      btnValidSkills.on('click', function(e){
        var id_cv = e.target.id;
        var cvS_name = $('#cvS_name').val();
        for (var i = cvS_details.length; i > 0; i--) {
           cvS_details.pop();
        }

        $( "#details input" ).each(function() {
          cvS_details.push($(this).val());
        });

        var infos = {
          id_cv : id_cv,
          id_cvS : id_cvS,
          cvS_name : cvS_name,
          cvS_detail : cvS_details
        };

        console.log("id_cvS : " + infos.id_cvS);
        console.log("cvS_detail : " + infos.cvS_detail);

        var urlPut = '/profile/cv/skills/:' + id_cv;
        var url = '/profile/cv/:' + id_cv
        $.ajax({
          url : urlPut,
          type : 'PUT',
          data : infos,
          success : function(req, res, data){
            if(res == 'success'){
              window.location.href = url;
              console.log(data);
            }
          }
        });
      });
    })
  };

  var deleteCvSkills = function(){
    var btnDeleteCvSkills = $('.btnDeleteCvSkills');

    btnDeleteCvSkills.on('click', function(e){
      var id_cvS = e.target.id;
      var id_cv = $('.btnAddCSkills').attr('id');

      var infos = {
        id_cv : id_cv,
        id_cvS : id_cvS,
      };

      var urlDelete = '/profile/cv/skills/:' + id_cv;
      var url = '/profile/cv/:' + id_cv
      $.ajax({
        url : urlDelete,
        type : 'DELETE',
        data : infos,
        success : function(req, res, data){
          if(res == 'success'){
            window.location.href = url;
          }
        }
      });
    });
  };

  return {
    moreDetails : moreDetails,
    postCvSkills : postCvSkills,
    updateCvSkills : updateCvSkills,
    deleteCvSkills : deleteCvSkills
  };

})();

cv_skills.moreDetails();
cv_skills.postCvSkills();
cv_skills.updateCvSkills();
cv_skills.deleteCvSkills();
