var profile = (function(){

  var consultCv = function(){
    var btnsConsultCv = $('.btnsConsultCv');

    btnsConsultCv.on('click', function(e){
      var id_cv = e.target.id;
      var urlGet = '/profile/cv/:' + id_cv;
      $.get(urlGet, function(req, res, data){
        console.log(res);
        console.log(data.responseText)
        if(res == 'success'){
          window.location.href = urlGet;
        }
      })
    })
  };

  return {
    consultCv : consultCv
  };

})();

profile.consultCv();
