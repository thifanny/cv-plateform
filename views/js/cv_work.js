var cv_work = (function(){

  // Post new cv_work
  var postCvWork = function(){
    var btnAddCvWork = $('.btnAddCvWork');

    btnAddCvWork.on('click', function(e){
      var id_cv = e.target.id;
      var btnValidPost = $('.btnValidPost');

      $('.btnValidPost').on('click', function(e){
        var id_cv = btnAddCvWork.attr('id');
        var cvW_jobTitle = $('#cvW_jobTitle').val();
        var cvW_jobStartDate = $('#cvW_jobStartDate').val();
        var cvW_jobFinishDate = $('#cvW_jobFinishDate').val();
        var cvW_jobCity = $('#cvW_jobCity').val();
        var cvW_jobContractType = $('#cvW_jobContractType').val();
        var cvW_jobDescription = $('#cvW_jobDescription').val();
        var cvW_jobDetails = $('#cvW_jobDetails').val();
        var cvW_companyName = $('#cvW_companyName').val();
        var cvW_companyDescription = $('#cvW_companyDescription').val();
        var cvW_companyWebsite = $('#cvW_companyWebsite').val();

        var infos = {
          cvW_jobTitle : cvW_jobTitle,
          cvW_jobStartDate : cvW_jobStartDate,
          cvW_jobFinishDate : cvW_jobFinishDate,
          cvW_jobCity : cvW_jobCity,
          cvW_jobContractType : cvW_jobContractType,
          cvW_jobDescription : cvW_jobDescription,
          cvW_jobDetails : cvW_jobDetails,
          cvW_companyName : cvW_companyName,
          cvW_companyDescription : cvW_companyDescription,
          cvW_companyWebsite : cvW_companyWebsite
        };

        var urlPost = '/profile/cv/work/:' + id_cv;
        var url = '/profile/cv/:' + id_cv
        $.post(urlPost, infos, function(req, res, data){
          console.log(res);
          console.log(data);
          if(res == 'success'){
            window.location.href = url;
          }
        });
      });
    });
  };

  var updateCvWork = function(){

    var btnUpdateCvWork = $('.btnUpdateCvWork');

    btnUpdateCvWork.on('click', function(e){
      var cvW_id = e.target.id;
      var btnValidPost = $('.btnValidPost');

      btnValidPost.on('click', function(e){
        var cv_id = e.target.id;
        var cvW_jobTitle = $('#cvW_jobTitle').val();
        var cvW_jobStartDate = $('#cvW_jobStartDate').val();
        var cvW_jobFinishDate = $('#cvW_jobFinishDate').val();
        var cvW_jobCity = $('#cvW_jobCity').val();
        var cvW_jobContractType = $('#cvW_jobContractType').val();
        var cvW_jobDescription = $('#cvW_jobDescription').val();
        var cvW_jobDetails = $('#cvW_jobDetails').val();
        var cvW_companyName = $('#cvW_companyName').val();
        var cvW_companyDescription = $('#cvW_companyDescription').val();
        var cvW_companyWebsite = $('#cvW_companyWebsite').val();

        var infos = {
          cvW_id : cvW_id,
          cvW_jobTitle : cvW_jobTitle,
          cvW_jobStartDate : cvW_jobStartDate,
          cvW_jobFinishDate : cvW_jobFinishDate,
          cvW_jobCity : cvW_jobCity,
          cvW_jobContractType : cvW_jobContractType,
          cvW_jobDescription : cvW_jobDescription,
          cvW_jobDetails : cvW_jobDetails,
          cvW_companyName : cvW_companyName,
          cvW_companyDescription : cvW_companyDescription,
          cvW_companyWebsite : cvW_companyWebsite
        };

        var urlPut = '/profile/cv/work/:' + cv_id;
        var url = '/profile/cv/:' + cv_id;

        $.ajax({
          url: urlPut,
          type: 'PUT',
          data: infos,
          success: function(req, res, data) {
            if(res == 'success'){
              window.location.href = url;
            }
          }
        });
      });
    });
  };

  var deleteCvWork = function(){
    var btnDeleteCvWork = $('.btnDeleteCvWork');

    btnDeleteCvWork.on('click', function(e){
      var info = { cvW_id : e.target.id };

      if(confirm('Etes-vous sûr de vouloir supprimer cette expérience ?')){
        var id_cv = $('.btnAddCvWork').attr('id');
        console.log(id_cv)
        var urlDelete = '/profile/cv/work/:' + id_cv;
        $.ajax({
          url: urlDelete,
          type: 'DELETE',
          data : info,
          success: function(req, res, data) {
            console.log(res);
            if(res == 'success'){
              window.location.href = '/profile/cv/:' + id_cv;
            }
          }
        });
      };
    });
  };

  return {
    postCvWork : postCvWork,
    deleteCvWork : deleteCvWork,
    updateCvWork : updateCvWork
  };

})();

cv_work.postCvWork();
cv_work.updateCvWork();
cv_work.deleteCvWork();
