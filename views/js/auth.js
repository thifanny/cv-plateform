var auth = (function(){

  var ctrl = function(){
    var regexPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    var regexEmail = /^.+@[^\.].*\.[a-z]{2,}$/;

    var password = $('.password');

    $('#emailLogin').on('blur', function() {
      var emailVal = $('#emailLogin').val();
      if(!regexEmail.test(emailVal)){
        $(this).css('border-color', 'red');
        $('.helpEmail').removeAttr('hidden');
      } else {
        $(this).css('border-color', 'green');
        $('.helpEmail').attr('hidden', 'true');
      }
    })

    $('#emailConnect').on('blur', function() {
      var emailVal = $('#emailConnect').val();
      if(!regexEmail.test(emailVal)){
        $(this).css('border-color', 'red');
        $('.helpEmail').removeAttr('hidden');
      } else {
        $(this).css('border-color', 'green');
        $('.helpEmail').attr('hidden', 'true');
      }
    })

    $('#passwordLogin').on('blur', function() {
      var passwordVal = $('#passwordLogin').val();
      if(!regexPassword.test(passwordVal)){
        $(this).css('border-color', 'red');
        $('.helpPassword').removeAttr('hidden');
      } else {
        $(this).css('border-color', 'green');
        $('.helpPassword').attr('hidden', 'true');
      }
    });

    $('#passwordConnect').on('blur', function() {
      var passwordVal = $('#passwordConnect').val();
      if(!regexPassword.test(passwordVal)){
        $(this).css('border-color', 'red');
        $('.helpPassword').removeAttr('hidden');
      } else {
        $(this).css('border-color', 'green');
        $('.helpPassword').attr('hidden', 'true');
      }
    })

  };

  return {
    ctrl : ctrl };

})();

auth.ctrl();
