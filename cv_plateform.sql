-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 31, 2019 at 03:45 PM
-- Server version: 5.7.24
-- PHP Version: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cv_plateform`
--

-- --------------------------------------------------------

--
-- Table structure for table `cv`
--

CREATE TABLE `cv` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cv_identity`
--

CREATE TABLE `cv_identity` (
  `id` int(11) NOT NULL,
  `id_cv` int(11) NOT NULL,
  `cvI_name` varchar(50) DEFAULT NULL,
  `cvI_email` varchar(100) DEFAULT NULL,
  `cvI_website` varchar(100) DEFAULT NULL,
  `cvI_phoneNumber` int(9) DEFAULT NULL,
  `cvI_profilPicture` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cv_skill`
--

CREATE TABLE `cv_skill` (
  `id` int(11) NOT NULL,
  `id_cv` int(11) NOT NULL,
  `cvS_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cv_skill_detail`
--

CREATE TABLE `cv_skill_detail` (
  `id` int(11) NOT NULL,
  `id_skill_detail` int(11) NOT NULL,
  `cvSD_detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cv_work`
--

CREATE TABLE `cv_work` (
  `id` int(11) NOT NULL,
  `id_cv` int(11) NOT NULL,
  `cvW_jobTitle` varchar(100) DEFAULT NULL,
  `cvW_jobStartDate` date DEFAULT NULL,
  `cvW_jobFinishDate` date DEFAULT NULL,
  `cvW_jobCity` varchar(100) DEFAULT NULL,
  `cvW_jobContractType` varchar(100) DEFAULT NULL,
  `cvW_jobDescription` text,
  `cvW_jobDetails` text,
  `cvW_companyName` varchar(100) DEFAULT NULL,
  `cvW_companyDescription` text,
  `cvW_companyWebsite` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_email` text NOT NULL,
  `user_password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_email`, `user_password`) VALUES
(1, 'thif@gmail.com', '$2b$05$bAy035qzck0x6MOY5SPh.uGW0NInR3MFdhYDpmwGJ0itVH4Q11y6u');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_idUser` (`id_user`);

--
-- Indexes for table `cv_identity`
--
ALTER TABLE `cv_identity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_idCv` (`id_cv`);

--
-- Indexes for table `cv_skill`
--
ALTER TABLE `cv_skill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_idS_idCv` (`id_cv`);

--
-- Indexes for table `cv_skill_detail`
--
ALTER TABLE `cv_skill_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_idSD_idS` (`id_skill_detail`);

--
-- Indexes for table `cv_work`
--
ALTER TABLE `cv_work`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_idW_idCv` (`id_cv`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cv`
--
ALTER TABLE `cv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cv_identity`
--
ALTER TABLE `cv_identity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cv_skill`
--
ALTER TABLE `cv_skill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cv_skill_detail`
--
ALTER TABLE `cv_skill_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cv_work`
--
ALTER TABLE `cv_work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cv`
--
ALTER TABLE `cv`
  ADD CONSTRAINT `fk_id_idUser` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `cv_identity`
--
ALTER TABLE `cv_identity`
  ADD CONSTRAINT `fk_id_idCv` FOREIGN KEY (`id_cv`) REFERENCES `cv` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv_skill`
--
ALTER TABLE `cv_skill`
  ADD CONSTRAINT `fk_idS_idCv` FOREIGN KEY (`id_cv`) REFERENCES `cv` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv_skill_detail`
--
ALTER TABLE `cv_skill_detail`
  ADD CONSTRAINT `fk_idSD_idS` FOREIGN KEY (`id_skill_detail`) REFERENCES `cv_skill` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv_work`
--
ALTER TABLE `cv_work`
  ADD CONSTRAINT `fk_idW_idCv` FOREIGN KEY (`id_cv`) REFERENCES `cv` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
