var mysql = require('mysql');
var dbconfig = require('../../config/database');

module.exports = {

  updateProfilePicture : function(data, callback){
    var id_user = data.id_user;
    var id_cv = data.id_cv;
    var cvI_profilPicture = data.cvI_profilPicture;

    var queryCheckUser = 'SELECT id_user FROM cv WHERE id= ?';
    var queryUpdateCvIdentity = 'UPDATE cv_identity SET cvI_profilPicture = ? \
    WHERE id_cv= ?';

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err){
        throw 'pb db'
      } else if(id_user != rows[0].id_user){
        throw 'pb user'
      } else {
        connection.query(queryUpdateCvIdentity, [cvI_profilPicture, id_cv], function(err, rows){
          if(err){
            throw err + 'pb db updateProfilePicture'
          } else {
            callback(rows)
          }
        });
      }
    });
  },

  // Update cv_identity
  update : function(data, callback){
    var id_user = data.id_user;
    var id_cv = data.id_cv;
    var cvI_name = data.cvI_name;
    var cvI_email = data.cvI_email;
    var cvI_website = data.cvI_website;
    var cvI_phoneNumber = data.cvI_phoneNumber;

    var queryCheckUser = 'SELECT id_user FROM cv WHERE id= ?';
    var queryUpdateCvIdentity = 'UPDATE cv_identity SET cvI_name = ?, \
    cvI_email = ?, cvI_website = ?, cvI_phoneNumber = ? WHERE id_cv= ?';

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err){
        throw 'pb db'
      } else if(id_user != rows[0].id_user){
        throw 'pb user'
      } else {
        connection.query(queryUpdateCvIdentity, [cvI_name, cvI_email, cvI_website, cvI_phoneNumber, id_cv], function(err, rows){
          if(err){
            throw err + 'pb db update'
          } else {
            callback(rows)
          }
        });
      }
    });
  },

}
