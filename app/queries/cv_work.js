var mysql = require('mysql');
var dbconfig = require('../../config/database');

module.exports = {

  create : function(data, callback){
    var id_user = data.id_user;
    var id_cv = data.id_cv;
    var cvW_jobTitle = data.cvW_jobTitle;
    var cvW_jobStartDate = data.cvW_jobStartDate;
    var cvW_jobFinishDate = data.cvW_jobFinishDate;
    var cvW_jobCity = data.cvW_jobCity;
    var cvW_jobContractType = data.cvW_jobContractType;
    var cvW_jobDescription = data.cvW_jobDescription;
    var cvW_jobDetails = data.cvW_jobDetails;
    var cvW_companyName = data.cvW_companyName;
    var cvW_companyDescription = data.cvW_companyDescription;
    var cvW_companyWebsite = data.cvW_companyWebsite;

    var queryCheckUser = 'SELECT id_user FROM cv WHERE id=?';
    var query = 'INSERT INTO cv_work(id_cv, cvW_jobTitle, cvW_jobStartDate, \
      cvW_jobFinishDate, cvW_jobCity, cvW_jobContractType, cvW_jobDescription, \
      cvW_jobDetails, cvW_companyName, cvW_companyDescription, cvW_companyWebsite) \
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err) {
        throw err
      }else if(id_user != rows[0].id_user){
        throw 'pb user'
      } else {
        connection.query(query, [id_cv, cvW_jobTitle, cvW_jobStartDate,
          cvW_jobFinishDate, cvW_jobCity, cvW_jobContractType, cvW_jobDescription,
          cvW_jobDetails, cvW_companyName, cvW_companyDescription, cvW_companyWebsite], function(err, rows){
          if(err){
            throw err
          }
          callback(rows);
        });
      }
    })
  },

  update : function(data, callback){
    var id_user = data.id_user;
    var id_cv = data.id_cv;
    var id_cvW_id = data.id_cvW_id;
    var cvW_jobTitle = data.cvW_jobTitle;
    var cvW_jobStartDate = data.cvW_jobStartDate;
    var cvW_jobFinishDate = data.cvW_jobFinishDate;
    var cvW_jobCity = data.cvW_jobCity;
    var cvW_jobContractType = data.cvW_jobContractType;
    var cvW_jobDescription = data.cvW_jobDescription;
    var cvW_jobDetails = data.cvW_jobDetails;
    var cvW_companyName = data.cvW_companyName;
    var cvW_companyDescription = data.cvW_companyDescription;
    var cvW_companyWebsite = data.cvW_companyWebsite;

    var queryCheckUser = 'SELECT id_user FROM cv WHERE id= ? ';
    var query = 'UPDATE cv_work SET cvW_jobTitle = ?, \
    cvW_jobStartDate = ?, cvW_jobFinishDate = ?, cvW_jobCity = ?, \
    cvW_jobContractType= ?, cvW_jobDescription = ?, cvW_jobDetails = ?, \
    cvW_companyName = ?, cvW_companyDescription = ?, cvW_companyWebsite = ? \
    WHERE id = ? ';

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err) {
        throw err
      }else if(id_user != rows[0].id_user){
        throw 'pb user'
      } else {
        connection.query(query, [cvW_jobTitle, cvW_jobStartDate, cvW_jobFinishDate,
          cvW_jobCity, cvW_jobContractType, cvW_jobDescription, cvW_jobDetails,
          cvW_companyName, cvW_companyDescription, cvW_companyWebsite, id_cvW_id], function(err, rows){
          if(err){
            throw err
          }
          callback(rows);
        });
      }
    })
  },

  delete : function(data, callback){

    var id_user = data.id_user;
    var id_cv = data.id_cv;
    var id_cvW_id = data.id_cvW_id;

    var queryCheckUser = 'SELECT id_user FROM cv WHERE id=?';
    var query = 'DELETE FROM cv_work WHERE id= ?';

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err) {
        throw err
      }else if(id_user != rows[0].id_user){
        throw 'pb user'
      } else {
        connection.query(query, [id_cvW_id], function(err, rows){
          if(err){
            throw err
          }
          callback(rows);
        });
      }
    })
  },

}
