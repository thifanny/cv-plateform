var mysql = require('mysql');
var dbconfig = require('../../config/database');

module.exports = {

  create : function(data, callback){
    var id_user = data.id_user;
    var id_cv = data.id_cv;
    var cvS_name = data.cvS_name;
    var cvS_detail = data.cvS_detail;

    var queryCheckUser = 'SELECT id_user FROM cv WHERE id=?';
    var queryName = 'INSERT INTO cv_skill(id_cv, cvS_name) VALUES (?, ?)';
    var queryDetails = 'INSERT INTO cv_skill_detail(id_skill_detail, cvSD_detail) VALUES(?, ?)'

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err) {
        throw err
      }else if(id_user != rows[0].id_user){
        throw 'pb queryCheckUser'
      } else {
        connection.query(queryName, [id_cv, cvS_name], function(err, rows){
          if(err){
            throw 'pb queryName'
          }
          var id_cvSkill = rows.insertId;
          console.log('rows.insertId : ' + id_cvSkill);

          for(var i = 0; i < cvS_detail.length; i++){
            var cvS_d = cvS_detail[i];
            connection.query(queryDetails, [id_cvSkill, cvS_d], function(err, rows){
              if(err){
                throw err + 'queryDetails'
              }
            });
          }
          callback(rows)
        });
      };
    });
  },

  update : function(data, callback){
    var id_user = data.id_user;
    var id_cv = data.id_cv;
    var id_cvS = data.id_cvS;
    var cvS_name = data.cvS_name;
    var cvS_detail = data.cvS_detail;

    console.log("id_cv : " + id_cv);
    console.log("id_cvS : " + id_cvS);
    console.log("cvS_name : " + cvS_name);
    console.log("cvS_detail : " + cvS_detail);

    var queryCheckUser = 'SELECT id_user FROM cv WHERE id= ? ';

    var deleteSkillDetail = 'DELETE FROM cv_skill_detail WHERE id_skill_detail = ?';
    var updateSkillName = 'UPDATE cv_skill SET cvS_name= ? WHERE id = ?';
    var insertSkillDetail = 'INSERT INTO cv_skill_detail(id_skill_detail, cvSD_detail) VALUES (?, ?)';

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err) {
        throw err
      }else if(id_user != rows[0].id_user){
        throw 'pb queryCheckUser'
      } else {
        connection.query(deleteSkillDetail, [id_cvS], function(err, rows){
          if(err) {
            throw err
          }
          connection.query(updateSkillName, [cvS_name, id_cvS], function(err, rows){
            if(err){
              throw 'pb queryName'
            }
            for(var i = 0; i < cvS_detail.length; i++){
              var cvS_d = cvS_detail[i];
              connection.query(insertSkillDetail, [id_cvS, cvS_d], function(err, rows){
                if(err){
                  throw err + 'queryDetails'
                }
              });
            }
            callback(rows)
          });
        });
      };
    });

  },

  delete : function(data, callback){

    var id_user = data.id_user;
    var id_cv = data.id_cv;
    var id_cvS = data.id_cvS;

    var queryCheckUser = 'SELECT id_user FROM cv WHERE id=?';
    var deleteSkill = 'DELETE FROM cv_skill WHERE id = ?';

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err) {
        throw err
      }else if(id_user != rows[0].id_user){
        throw 'pb user'
      } else {
        connection.query(deleteSkill, [id_cvS], function(err, rows){
          if(err){
            throw err
          }
          callback(rows);
        });
      }
    });
  }

}
