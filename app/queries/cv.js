var mysql = require('mysql');
var dbconfig = require('../../config/database');

module.exports = {

  // List of titles to ProfilePage
  retrieveAll : function(id_user, callback){
    var id_user = id_user;
    var query = 'SELECT title, id FROM cv WHERE id_user = ?';
    connection.query(query, [id_user], function(err, rows, fields){
      if (err) {
        throw err
      }
      callback(rows);
    })
  },

  //List of cv'data(cv_title + cv_identity + cv_work + cv_skill + cv_skill_detail) to CvPage
  retrieve : function(data, callback){
    var id_cv = data.id_cv;
    var id_user = data.id_user;

    var queryCvAll = 'SELECT id, title FROM cv WHERE id_user = ?'
    var queryCv = 'SELECT id_user, title FROM cv WHERE id= ?';
    var queryIdentity = 'SELECT cvI_name, cvI_email, cvI_website, cvI_phoneNumber, \
    cvI_profilPicture FROM cv_identity WHERE id_cv = ?';
    var queryWork = 'SELECT cv_work.id AS cvW_id, cv_work.cvW_jobTitle, \
    DATE_FORMAT(cv_work.cvW_jobStartDate, "%d/%m/%Y") AS cvW_jobStartDate, \
    DATE_FORMAT(cv_work.cvW_jobFinishDate,  "%d/%m/%Y") AS cvW_jobFinishDate, \
    cv_work.cvW_jobCity, cv_work.cvW_jobContractType, cv_work.cvW_jobDescription, \
    cv_work.cvW_jobDetails, cv_work.cvW_companyName, cv_work.cvW_companyDescription, \
    cv_work.cvW_companyWebsite FROM cv_work WHERE id_cv = ?';
    var querySkill = 'SELECT id, cvS_name FROM cv_skill WHERE id_cv = ?';
    var querySkillDetail = 'SELECT cv_skill_detail.id, cv_skill_detail.id_skill_detail, \
    cvSD_detail FROM cv_skill \
    LEFT JOIN cv_skill_detail ON cv_skill.id = cv_skill_detail.id_skill_detail \
    WHERE cv_skill.id_cv = ?';

    connection.query(queryCv, [id_cv], function(err, rows){
      if(err){
        throw err
      } else if(id_user != rows[0].id_user) {
        throw err + 'pbUser';
      }
      var cv = rows[0];
      connection.query(queryCvAll, [id_user], function(err, rows){
        if(err){
          throw err
        }
        var cvAll = rows;
        connection.query(queryWork, [id_cv], function(err, rows){
          if(err){
            throw err
          }
          var work = rows;
          connection.query(queryIdentity, [id_cv], function(err, rows){
            if(err){
              throw err
            }
            var identity = rows[0];
            connection.query(queryWork, [id_cv], function(err, rows){
              if(err){
                throw err
              }
              var work = rows;
              connection.query(querySkill, [id_cv], function(err, rows){
                if(err){
                  throw err
                }
                var skill = rows;
                connection.query(querySkillDetail, [id_cv], function(err, rows){
                  if(err){
                    throw err
                  }
                  var skill_detail = rows;
                  callback(cv, cvAll, identity, work, skill, skill_detail);
                });
              });
            });
          });
        })
      })
    });
  },

  //Create cv
  create : function(data, callback){
    var id_user = data.id_user;
    var title = data.title;
    var query = 'INSERT INTO cv(id_user, title) VALUES (?, ?)';
    var query_cvIdentity = 'INSERT INTO cv_identity(id_cv) VALUES (?)';

    connection.query(query, [id_user, title], function(err, rows){
      if(err) {
        throw err
      }
      var id_cv = rows.insertId;
      connection.query(query_cvIdentity, [id_cv], function(err, rows){
        if(err){
          throw err
        }
      });
      callback(rows, id_cv);
    })
  },

  // Update cv_title
  updateTitle : function(data, callback){
    var id_user = data.id_user;
    var id_cv = data.id_cv;
    var cv_title = data.cv_title;

    var queryCheckUser = 'SELECT id_user, title FROM cv WHERE id=?';
    var queryUpdateTitle = 'UPDATE cv SET title = ? WHERE id=?';

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err){
        throw 'pb db'
      } else if(id_user != rows[0].id_user){
        throw 'pb user'
      } else {
        connection.query(queryUpdateTitle, [cv_title, id_cv], function(err, rows){
          if(err){
            throw 'pb db 2'
          } else{
            connection.query(queryCheckUser, [id_cv], function(err, rows){
              if(err){
                throw 'pb db 3'
              } else {
                var infos = {
                  cv_title : rows[0].title
                }
                callback(rows);
              }
            })
          }
        })
      }
    });
  },

  // Delete cv and cv_identity
  delete : function(id_user, id_cv, callback){
    var id_user = id_user;
    var id_cv = id_cv;
    var queryCheckUser = 'SELECT id_user FROM cv WHERE id= ?'
    var queryDeleteCv = 'DELETE FROM cv WHERE id= ?';

    connection.query(queryCheckUser, [id_cv], function(err, rows){
      if(err){
        throw err
      } else if(id_user != rows[0].id_user) {
        callback(err)
      }
      connection.query(queryDeleteCv, [id_cv], function(err, rows, fields){
        if(err){
          throw err
        }
        callback(rows);
      })
    })
  },

}
