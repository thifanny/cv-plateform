var cv_skills = require('../queries/cv_skills');

module.exports = {

  create : function(req, res){
    var id_cv = parseInt(req.params.id.slice(1));
    var id_user = req.user.id;

    var data = {
      id_cv : id_cv,
      id_user : id_user,
      cvS_name : req.body.cvS_name,
      cvS_detail : req.body.cvS_detail
    };

    cv_skills.create(data, function(rows){
      res.send('ok');
    });
  },

  update : function(req, res){
    var id_cv = parseInt(req.params.id.slice(1));
    var id_user = req.user.id;

    var data = {
      id_cv : id_cv,
      id_user : id_user,
      id_cvS : req.body.id_cvS,
      cvS_name : req.body.cvS_name,
      cvS_detail : req.body.cvS_detail
    };

    console.log("id_cv : " + data.id_cv);
    console.log("id_cvS : " + data.id_cvS);
    console.log("cvS_name : " + data.cvS_name);
    console.log("cvS_detail : " + data.cvS_detail);

    cv_skills.update(data, function(rows){
      res.send('ok');
    });
  },

  delete : function(req, res){
    var id_cv = parseInt(req.params.id.slice(1));
    var id_user = req.user.id;

    var data = {
      id_cv : id_cv,
      id_user : id_user,
      id_cvS : parseInt(req.body.id_cvS)
    };

    console.log("id_cv : " + data.id_cv);
    console.log("id_cvS : " + data.id_cvS);

    cv_skills.delete(data, function(rows){
      res.send('ok');
    });

  }

};
