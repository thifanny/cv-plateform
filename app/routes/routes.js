var authCtrl = require('./authCtrl')
var cvCtrl = require('./cvCtrl');
var cvICtrl = require('./cvICtrl');
var cvWCtrl = require('./cvWCtrl');
var cvSCtrl = require('./cvSCtrl');

module.exports = function(app, passport){

  //Homepage
  app.get('/', function(req, res){
    res.render('index.pug');
  });

  app.get('/signup', function(req, res){
    res.render('index.pug', { message: req.flash('signupMessage') })
  });

  //Signup
  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect : '/profile',
    failureRedirect : '/signup',
    failureFlash : true
  }));

  app.get('/login', function(req, res){
    res.render('index.pug', { message: req.flash('loginMessage') })
  });

  //Login (authCtrl)
  app.post('/login', passport.authenticate('local-login', {
    successRedirect: '/profile',
    failureRedirect: '/login',
    failureFlash : true
  }), authCtrl.login);

  //Logout (authCtrl)
  app.get('/logout', authCtrl.logout);

  // CV
  app.get('/profile', isLoggedIn, cvCtrl.retrieveAll);
  app.get('/profile/cv/:id', isLoggedIn, cvCtrl.retrieve);
  app.post('/profile/cv/add', isLoggedIn, cvCtrl.create);
  app.put('/profile/cv/title/:id', isLoggedIn, cvCtrl.update);
  app.delete('/profile/cv/:id', isLoggedIn, cvCtrl.delete);

  // CV_identity
  app.put('/profile/cv/:id', isLoggedIn, cvICtrl.update);
  app.put('/profile/cv/photo/:id',  isLoggedIn, cvICtrl.updateProfilePicture)
  // CV_work
  app.post('/profile/cv/work/:id', isLoggedIn, cvWCtrl.create);
  app.put('/profile/cv/work/:id', isLoggedIn, cvWCtrl.update);
  app.delete('/profile/cv/work/:id', isLoggedIn, cvWCtrl.delete);

  // CV_skills
  app.post('/profile/cv/skills/:id', isLoggedIn, cvSCtrl.create);
  app.put('/profile/cv/skills/:id', isLoggedIn, cvSCtrl.update);
  app.delete('/profile/cv/skills/:id', isLoggedIn, cvSCtrl.delete);

  //Verify authentication User
  function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
      return next();
    }
    res.redirect('/');
  };
};
