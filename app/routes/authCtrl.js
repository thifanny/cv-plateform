var mysql = require('mysql');
var bcrypt = require('bcrypt');
var dbconfig = require('../../config/database');

module.exports = {

  login : function(req, res) {
    if (req.body.remember) {
      req.session.cookie.maxAge = 30 * 24 * 60 * 60 * 1000; // Cookie expires after 30 days
    } else {
      req.session.cookie.expires = false; // Cookie expires at end of session
    }
    res.redirect('/');
  },

  logout : function(req, res){
   req.logout();
   res.redirect('/');
  },

};
