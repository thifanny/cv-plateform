var cv_work = require('../queries/cv_work');

module.exports = {

  create : function(req, res){
    var id_cv = req.params.id.slice(1);
    var id_user = req.user.id;

    var cvW_jobStartDate = req.body.cvW_jobStartDate;
    var cvW_jobFinishDate = req.body.cvW_jobFinishDate;

    if (cvW_jobStartDate = ' '){
      cvW_jobStartDate = null
    }

    if (cvW_jobFinishDate = ' '){
      cvW_jobFinishDate = null
    }

    var data = {
      id_cv : id_cv,
      id_user : id_user,
      id_cvW_id : req.body.cvW_id,
      cvW_jobTitle : req.body.cvW_jobTitle,
      cvW_jobStartDate : cvW_jobStartDate,
      cvW_jobFinishDate : cvW_jobFinishDate,
      cvW_jobCity : req.body.cvW_jobCity,
      cvW_jobContractType : req.body.cvW_jobContractType,
      cvW_jobDescription : req.body.cvW_jobDescription,
      cvW_jobDetails : req.body.cvW_jobDetails,
      cvW_companyName : req.body.cvW_companyName,
      cvW_companyDescription : req.body.cvW_companyDescription,
      cvW_companyWebsite : req.body.cvW_companyWebsite
    };

    cv_work.create(data, function(rows){
      res.end();
    });
  },

  update : function(req, res){
    var id_cv = req.params.id.slice(1);
    var id_user = req.user.id;
    var cvW_jobStartDate = req.body.cvW_jobStartDate;
    var cvW_jobFinishDate = req.body.cvW_jobFinishDate;

    if (!cvW_jobStartDate){
      cvW_jobStartDate = null
    }

    if (!cvW_jobFinishDate){
      cvW_jobFinishDate = null
    }

    var data = {
      id_user : id_user,
      id_cv : id_cv,
      id_cvW_id : parseInt(req.body.cvW_id),
      cvW_jobTitle : req.body.cvW_jobTitle,
      cvW_jobStartDate : cvW_jobStartDate,
      cvW_jobFinishDate : cvW_jobFinishDate,
      cvW_jobCity : req.body.cvW_jobCity,
      cvW_jobContractType : req.body.cvW_jobContractType,
      cvW_jobDescription : req.body.cvW_jobDescription,
      cvW_jobDetails : req.body.cvW_jobDetails,
      cvW_companyName : req.body.cvW_companyName,
      cvW_companyDescription : req.body.cvW_companyDescription,
      cvW_companyWebsite : req.body.cvW_companyWebsite
    };

    cv_work.update(data, function(rows){
      res.end();
    });
  },

  delete : function(req, res){
    var id_cv = req.params.id.slice(1);
    var id_user = req.user.id;
    var id_cvW_id = parseInt(req.body.cvW_id);

    var data = {
      id_user : id_user,
      id_cv : id_cv,
      id_cvW_id : id_cvW_id
    };

    cv_work.delete(data, function(rows){
      res.end();
    });
  }
};
