var cv_identity = require('../queries/cv_identity');
var fs = require('fs');

module.exports = {

  updateProfilePicture : function(req, res){
    var id_cv = parseInt(req.params.id.slice(1));
    var id_user = req.user.id;
    var cvI_profilPicture = req.body.cvI_profilPicture;
    var data = {
      id_cv : id_cv,
      id_user : id_user,
      cvI_profilPicture : cvI_profilPicture
    };

    cv_identity.updateProfilePicture(data, function(rows){
      res.end();
    });
  },

  update : function(req, res){
    var id_cv = parseInt(req.params.id.slice(1));
    var id_user = req.user.id;
    var cvI_phoneNumber = parseInt(req.body.cvI_phoneNumber);

    if(!cvI_phoneNumber || typeof(cvI_phoneNumber) != 'number'){
      cvI_phoneNumber = null;
    }

    var data = {
      id_cv : id_cv,
      id_user : id_user,
      cvI_name : req.body.cvI_name,
      cvI_email : req.body.cvI_email,
      cvI_website : req.body.cvI_website,
      cvI_phoneNumber : cvI_phoneNumber
    };

    cv_identity.update(data, function(rows){
      res.end();
    });
  }
};
