var cv = require('../queries/cv');

module.exports = {

  retrieveAll : function(req, res){
    var id_user = req.user.id;

    cv.retrieveAll(id_user, function(rows){
      res.render('profile.pug', {
        user : req.user,
        id_user : id_user,
        cvAll : rows
      });
    });
  },

  retrieve : function(req, res){
    var id_cv = req.params.id.slice(1);
    var id_user = req.user.id;

    var data = {
      id_cv : id_cv,
      id_user : id_user
    }

    cv.retrieve(data, function(cv, cvAll, identity, work, skill, skill_detail){
      res.render('cv.pug', {
        id_user : id_user,
        id_cv : id_cv,
        cvAll : cvAll,
        cv : cv,
        identity : identity,
        work : work,
        skill : skill,
        skill_detail : skill_detail
      });
    })
  },

  create : function(req, res){
    var id_user = req.user.id;

    var data = {
      id_user : id_user,
      title : req.body.title
    }

    cv.create(data, function(rows, id_cv){
      res.redirect('/profile/cv/:' + id_cv);
    });
  },

  update : function(req, res){
    var id_cv = req.params.id.slice(1);
    var id_user = req.user.id;

    var data = {
      id_cv : id_cv,
      id_user : id_user,
      cv_title : req.body.cv_title
    }

    cv.updateTitle(data, function(rows){
      res.end();
    });
  },

  delete : function(req, res){
    var id_cv = req.params.id.slice(1);
    var id_user = req.user.id;

    cv.delete(id_user, id_cv, function(rows){
      if(rows.affectedRows >= 1){
        res.status(200).end();
      }
    })
  }  

};
